#include "stdafx.h"
#include "Board.h"


Board::Board(const int width, const int height, bool* board)
{
	this->width = width;
	this->height = height;
	this->board = board;

	tempBoard = this->board + (width) * (height);
}

Board::~Board()
{
	deleteBoard(board);
}

void Board::IterateBoard()
{
	#pragma omp parallel
	{
		// left - up corner (0,0)
		uint_fast8_t liveNeighbors = board[getIndex(width - 1, height - 1)] + board[getIndex(0, height - 1)] + board[getIndex(1, height - 1)] + board[getIndex(width - 1, 0)]
			+ board[getIndex(1, 0)] + board[getIndex(width - 1, 1)] + board[getIndex(0, 1)] + board[getIndex(1, 1)];

		setBoard(0, liveNeighbors);

		// upper border (1-(width-2), 0)
		#pragma omp for nowait
		for (int x = 1; x < width - 1; x++)
		{
			liveNeighbors = board[getIndex(x - 1, height - 1)] + board[getIndex(x, height - 1)] + board[getIndex(x + 1, height - 1)] + board[getIndex(x - 1, 0)]
				+ board[getIndex(x + 1, 0)] + board[getIndex(x - 1, 1)] + board[getIndex(x, 1)] + board[getIndex(x + 1, 1)];

			setBoard(getIndex(x, 0), liveNeighbors);
		}

		// right - up corner (width-1, 0)
		liveNeighbors = board[getIndex(width - 2, height - 1)] + board[getIndex(width - 1, height - 1)] + board[getIndex(0, height - 1)] + board[getIndex(width - 2, 0)]
			+ board[getIndex(0, 0)] + board[getIndex(width - 2, 1)] + board[getIndex(width - 1, 1)] + board[getIndex(0, 1)];

		setBoard(getIndex(width - 1, 0), liveNeighbors);

		// left border (0, 1-height-2)
		#pragma omp for nowait
		for (int y = 1; y < height - 1; y++)
		{
			liveNeighbors = board[getIndex(width - 1, y - 1)] + board[getIndex(0, y - 1)] + board[getIndex(1, y - 1)] + board[getIndex(width - 1, y)]
				+ board[getIndex(1, y)] + board[getIndex(width - 1, y + 1)] + board[getIndex(0, y + 1)] + board[getIndex(1, y + 1)];

			setBoard(getIndex(0, y), liveNeighbors);
		}

		// right border (width-1, 1)
		#pragma omp for nowait
		for (int y = 1; y < height - 1; y++)
		{
			liveNeighbors = board[getIndex(width - 2, y - 1)] + board[getIndex(width - 1, y - 1)] + board[getIndex(0, y - 1)] + board[getIndex(width - 2, y)]
				+ board[getIndex(0, y)] + board[getIndex(width - 2, y + 1)] + board[getIndex(width - 1, y + 1)] + board[getIndex(0, y + 1)];

			setBoard(getIndex(width - 1, y), liveNeighbors);
		}

		// center
		const int length = (height - 1)*(width - 1);
		for (int n = 1; n < length; n++)
		{
			int y = n / (width - 1);
			int x = n % (width - 1);
			liveNeighbors = board[getIndex(x - 1, y - 1)] + board[getIndex(x, y - 1)] + board[getIndex(x + 1, y - 1)] + board[getIndex(x - 1, y)]
				+ board[getIndex(x + 1, y)] + board[getIndex(x - 1, y + 1)] + board[getIndex(x, y + 1)] + board[getIndex(x + 1, y + 1)];

			setBoard(getIndex(x, y), liveNeighbors);
		}
		/*for (int y = 1; y < height - 1; y++)
		{
			for (int x = 1; x < width - 1; x++)
			{
				liveNeighbors = board[getIndex(x - 1, y - 1)] + board[getIndex(x, y - 1)] + board[getIndex(x + 1, y - 1)] + board[getIndex(x - 1, y)]
					+ board[getIndex(x + 1, y)] + board[getIndex(x - 1, y + 1)] + board[getIndex(x, y + 1)] + board[getIndex(x + 1, y + 1)];

				setBoard(getIndex(x, y), liveNeighbors);
			}
		}*/

		// left - bottom corner (0, height-1)
		liveNeighbors = board[getIndex(width - 1, height - 2)] + board[getIndex(0, height - 2)] + board[getIndex(1, height - 2)] + board[getIndex(0, 0)]
			+ board[getIndex(width - 1, height - 1)] + board[getIndex(1, height - 1)] + board[getIndex(width - 1, 0)] + board[getIndex(1, 0)];

		setBoard(getIndex(0, height - 1), liveNeighbors);

		// bottom border (1-(width-1), height-1)
		#pragma omp for nowait
		for (int x = 1; x < width - 1; x++)
		{
			liveNeighbors = board[getIndex(x - 1, height - 2)] + board[getIndex(x, height - 2)] + board[getIndex(x + 1, height - 2)] + board[getIndex(x - 1, height - 1)]
				+ board[getIndex(x + 1, height - 1)] + board[getIndex(x - 1, 0)] + board[getIndex(x, 0)] + board[getIndex(x + 1, 0)];

			setBoard(getIndex(x, height - 1), liveNeighbors);
		}

		// right - bottom corner (width-1, height-1)
		liveNeighbors = board[getIndex(width - 2, height - 2)] + board[getIndex(width - 1, height - 2)] + board[getIndex(0, height - 2)] + board[getIndex(width - 2, height - 1)]
			+ board[getIndex(0, height - 1)] + board[getIndex(width - 2, 0)] + board[getIndex(width - 1, 0)] + board[getIndex(0, 0)];

		setBoard(getIndex(width - 1, height - 1), liveNeighbors);
	}

	bool* temp = board;
	board = tempBoard;
	tempBoard = temp;
}

void Board::setBoard(int index, int liveNeighbors) const
{
	char current = board[index];

	if (liveNeighbors == 3)
	{
		tempBoard[index] = true;
		return;
	}

	if (liveNeighbors > 3)
	{
		tempBoard[index] = false;
		return;
	}

	if (liveNeighbors <= 1)
	{
		tempBoard[index] = false;
		return;
	}

	if (current == true)
	{
		tempBoard[index] = true;
	}
	else
	{
		tempBoard[index] = false;
	}
}

void Board::deleteBoard(bool* board)
{
	if (board != nullptr)
	{
		delete[] board;
	}
}
