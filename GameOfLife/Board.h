#pragma once

#include <string>
#include <iostream>
#include <assert.h>
#include <omp.h>
using namespace std;

const char alive = 'x';
const char dead = '.';

class Board
{
private:
	bool* board = nullptr;
	bool* tempBoard = nullptr;

	int width = 0;
	int height = 0;
	
	void setBoard(int index, int liveNeighbors) const;
	void deleteBoard(bool* board);

	// Allows mod with negative numbers
	int mod (int a, int b)
	{
		return (a%b + b) % b;
	}
public:
	explicit Board(int width, int height, bool* board);
	~Board();
	void IterateBoard();

	int getIndex(const int x, const int y) const
	{
		return (y * width) + x;
	}

	int getWidth() const
	{
		return width;
	}
	int getHeight() const
	{
		return height;
	}
	bool* getBoardArray() const
	{
		return board;
	}
};

