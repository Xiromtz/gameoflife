#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include "Board.h"
using namespace std;

class FileManager
{
public:
	explicit FileManager();
	~FileManager();
	Board* readFile(const char* filePath) const;
	void saveFile(const char* filePath, Board* board);
};