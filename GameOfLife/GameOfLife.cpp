// GameOfLife.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Board.h"
#include "FileManager.h"
#include <chrono>
#include <ctime>
#include <iomanip>

using namespace std;
using namespace std::chrono;

int main(int argc, char* argv[])
{
	if (argc != 8)
		return EXIT_FAILURE;

	char* loadFilePath = nullptr;
	char* saveFilePath = nullptr;
	int generations = 0;
	bool shouldMeasure = false;

	for (int i = 0; i < argc; i++)
	{
		if (strcmp(argv[i], "--load") == 0)
		{
			loadFilePath = argv[++i];
		}
		else if (strcmp(argv[i], "--save") == 0)
		{
			saveFilePath = argv[++i];
		}
		else if (strcmp(argv[i], "--generations") == 0)
		{
			generations = strtol(argv[++i], nullptr, 10);
		}
		else if (strcmp(argv[i], "--measure") == 0)
		{
			shouldMeasure = true;
		}
	}

	high_resolution_clock::time_point fileStart = high_resolution_clock::now();

	FileManager fileManager;
	Board* board = fileManager.readFile(loadFilePath);
	if (board == nullptr)
		return EXIT_FAILURE;

	high_resolution_clock::time_point fileEnd = high_resolution_clock::now();
	nanoseconds loadTime = fileEnd - fileStart;

	auto loadHours = duration_cast<duration<int, ratio<3600>>>(loadTime);
	loadTime -= loadHours;
	auto loadMinutes = duration_cast<duration<int, ratio<60>>>(loadTime);
	loadTime -= loadMinutes;
	auto loadSeconds = duration_cast<duration<int, ratio<1>>>(loadTime);
	loadTime -= loadSeconds;
	auto loadMilliseconds = duration_cast<duration<int, milli>>(fileEnd - fileStart);

	high_resolution_clock::time_point generationStart = high_resolution_clock::now();

	for (int i = 0; i < generations; i++)
	{
		board->IterateBoard();
	}

	high_resolution_clock::time_point generationEnd = high_resolution_clock::now();
	nanoseconds generationTime = generationEnd - generationStart;

	auto generationHours = duration_cast<duration<int, ratio<3600>>>(generationTime);
	generationTime -= generationHours;
	auto generationMinutes = duration_cast<duration<int, ratio<60>>>(generationTime);
	generationTime -= generationMinutes;
	auto generationSeconds = duration_cast<duration<int, ratio<1>>>(generationTime);
	generationTime -= generationSeconds;
	auto generationMilliseconds = duration_cast<duration<int, milli>>(generationTime);

	high_resolution_clock::time_point saveStart = high_resolution_clock::now();

	fileManager.saveFile(saveFilePath, board);
	delete board;
	
	high_resolution_clock::time_point saveEnd = high_resolution_clock::now();
	nanoseconds saveTime = saveEnd - saveStart;

	auto saveHours = duration_cast<duration<int, ratio<3600>>>(saveTime);
	saveTime -= saveHours;
	auto saveMinutes = duration_cast<duration<int, ratio<60>>>(saveTime);
	saveTime -= saveMinutes;
	auto saveSeconds = duration_cast<duration<int, ratio<1>>>(saveTime);
	saveTime -= saveSeconds;
	auto saveMilliseconds = duration_cast<duration<int, milli>>(saveTime);
	
	cout << setw(2) << setfill('0') << loadHours.count();
	cout << ":";
	cout << setw(2) << setfill('0') << loadMinutes.count();
	cout << ":";
	cout << setw(2) << setfill('0') << loadSeconds.count();
	cout << ".";
	cout << setw(3) << setfill('0') << loadMilliseconds.count();
	cout << ";";
	
	cout << setw(2) << setfill('0') << generationHours.count();
	cout << ":";
	cout << setw(2) << setfill('0') << generationMinutes.count();
	cout << ":";
	cout << setw(2) << setfill('0') << generationSeconds.count();
	cout << ".";
	cout << setw(3) << setfill('0') << generationMilliseconds.count();
	cout << ";";
	cout << setw(2) << setfill('0') << saveHours.count();
	cout << ":";
	cout << setw(2) << setfill('0') << saveMinutes.count();
	cout << ":";
	cout << setw(2) << setfill('0') << saveSeconds.count();
	cout << ".";
	cout << setw(3) << setfill('0') << saveMilliseconds.count();
	cout << ";" << endl;
    return 0;
}

