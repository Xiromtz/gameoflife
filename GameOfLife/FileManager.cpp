#include "stdafx.h"
#include "FileManager.h"


FileManager::FileManager()
{
}


FileManager::~FileManager()
{
}

Board* FileManager::readFile(const char* filePath) const
{
	Board* board = nullptr;

	ifstream file(filePath);
	if (file.is_open())
	{
		bool* boardArray = nullptr;
		string line;
		int currentLine = 0;
		int width = 0, height = 0;

		while (getline(file, line))
		{
			if (currentLine == 0)
			{
				const int index = line.find(',');
				const string widthString = line.substr(0, index);
				const string heightString = line.substr(index + 1, line.length());

				width = stoi(widthString);
				height = stoi(heightString);

				boardArray = new bool[height*width*2];
			}
			else
			{
				bool* arr = new bool[width];
				for (int i = 0; i < width; i++)
				{
					arr[i] = line[i] == alive ? 1 : 0;
				}
				memcpy(boardArray + ((height - 1) - (currentLine - 1)) * width, arr, width);
				delete[] arr;
			}
			currentLine++;
		}

		file.close();
		board = new Board(width, height, boardArray);
	}

	return board;
}

void FileManager::saveFile(const char* filePath, Board* board)
{
	ofstream file(filePath);
	if (file.is_open())
	{
		const int width = board->getWidth();
		const int height = board->getHeight();
		bool* boardArray = board->getBoardArray();

		file << width << ',' << height << endl;
		for (int i = height-1; i >= 0; i--)
		{
			for (int u = 0; u < width; u++)
			{
				file << (boardArray[(i*(width)) + u] ? alive : dead);
			}
			file << '\n';
		}
		file.close();
	}
}